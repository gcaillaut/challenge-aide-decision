$(document).on('shiny:connected', function (e) {
    $(document).keyup(function (event) {
        if (event.keyCode == 13) {
            var focus_ok = $('#user_email').is(':focus') || $('#user_password').is(':focus');
            if (focus_ok) {
                $('#auth_button').click();
            }
        }
    });
});