# Challenge aide à la décision

Ce dépôt contient un ensemble d'images Docker destinées au challenge d'aide à la décision de l'IUT informatique d'Orléans.

# Utilisation

## Application web

Les variables présentes dans le fichier *shinyapp/server/settings.R* peuvent être modifié de manière à changer le comportement de l'application web. On peut notamment y spécifier le jeu de données de test qui permet d'évaluer les étudiants.

## Base de données

Le fichier *database/example/initial_data.sql* indique comment insérer un utilisateur étudiant et/ou administrateur. Il est possible d'insérer des données à la création du conteneur Docker en ajoutant des fichiers SQL dans le dossier *database/docker-entrypoint-initdb.d*. Si le conteneur est déjà créer, alors il est nécessaire d'exécuter un script au sein du conteneur, par exemple via la commande suivante (postgres est l'utilisateur par défaut de la base de données) :

```{shell}
docker-compose exec database psql -U postgres
```

Cette commande ouvre un terminal dans lequel il est possible d'exécuter des requêtes SQL afin de modifier l'état de la base de données.

## Lancement

```{shell}
docker-compose up
```
