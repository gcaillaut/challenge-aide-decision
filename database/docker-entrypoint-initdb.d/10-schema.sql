create extension if not exists pgcrypto;

create table Teams
(
    id serial primary key not null,
    name text unique not null
);

create table Users
(
    id serial primary key not null,
    firstname text not null,
    lastname text not null,
    email text unique not null,
    password text not null,
    admin boolean not null default false,
    team integer references Teams(id) on delete cascade
);

create table Submissions
(
    id serial primary key not null,
    date timestamp with time zone not null,
    status text not null,
    team integer references Teams(id) on delete cascade
);

create table Classifications
(
    id serial primary key not null,
    file text not null,
    expected text not null,
    predicted text,
    status text,
    submission integer references Submissions(id) on delete cascade
);

create table Stats
(
    id serial primary key not null,
    type text not null,
    value numeric not null,
    submission integer references Submissions(id) on delete cascade
);
