-- ATTENTION
-- Il faut remplacer les « id_equipe » par l'identifiant de l'équipe de l'utilisateur

-- ETUDIANT
insert into Teams (name) values ('Nom de l''équipe');
insert into Users (firstname, lastname, email, password, admin, team) values ('Gaëtan', 'Caillaut', 'gaetan.caillaut@etu.univ-orleans.fr', crypt('1234', gen_salt('bf')), false, id_equipe);

-- ADMIN
insert into Teams (name) values ('Enseignants');
insert into Users (firstname, lastname, email, password, admin, team) values ('Gaëtan', 'Caillaut', 'gaetan.caillaut@univ-orleans.fr', crypt('1234', gen_salt('bf')), true, id_equipe);
